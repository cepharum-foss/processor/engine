import { afterEach, beforeEach, describe, expect, it, vi } from "vitest";
import { useTermsHydration } from "@/composables/terms-hydration.js";
import { computed, isReactive, nextTick, toRef, watch, watchEffect } from "vue";

describe( "Terms hydration", () => {
	const errorLog = [];

	beforeEach( () => {
		errorLog.splice( 0 );

		vi.spyOn( console, "error" ).mockImplementation( ( ...args ) => {
			errorLog.push( args );
		} );
	} );

	afterEach( () => {
		vi.restoreAllMocks();
	} );

	it( "can be used basically", () => {
		expect( () => useTermsHydration( {} ) ).not.toThrowError();
	} );

	it( "rejects to work with nullish source", () => {
		expect( () => useTermsHydration() ).toThrowError();
		expect( () => useTermsHydration( undefined ) ).toThrowError();
		expect( () => useTermsHydration( null ) ).toThrowError();

		expect( errorLog ).toHaveLength( 3 );
	} );

	it( "rejects to work with scalar source", () => {
		expect( () => useTermsHydration( 0 ) ).toThrowError();
		expect( () => useTermsHydration( 1 ) ).toThrowError();
		expect( () => useTermsHydration( -123.45 ) ).toThrowError();
		expect( () => useTermsHydration( false ) ).toThrowError();
		expect( () => useTermsHydration( true ) ).toThrowError();
		expect( () => useTermsHydration( "" ) ).toThrowError();
		expect( () => useTermsHydration( "=1+1" ) ).toThrowError();

		expect( errorLog ).toHaveLength( 7 );
	} );

	it( "rejects to work with array source", () => {
		expect( () => useTermsHydration( [] ) ).toThrowError();
		expect( () => useTermsHydration( ["=1+1"] ) ).toThrowError();

		expect( errorLog ).toHaveLength( 2 );
	} );

	it( "rejects to work with function source", () => {
		expect( () => useTermsHydration( () => ( {} ) ) ).toThrowError();

		expect( errorLog ).toHaveLength( 1 );
	} );

	it( "provides reactive deep copy of provided object", () => {
		const { code } = useTermsHydration( {
			outer: true,
			inner: {
				string: "foo"
			}
		} );

		expect( isReactive( code ) ).toBeTruthy();
		expect( code ).toEqual( { outer: true, inner: { string: "foo" } } );

		expect( errorLog ).toHaveLength( 0 );
	} );

	it( "excludes prototype pollution attempts from provided reactive deep copy", () => {
		const source = {
			outer: true,
			constructor: {
				staticInjected: "INJECTED",
			},
			prototype: {
				injected: "injected",
			},
			__proto__: {
				INJECTED: "Injected",
			},
			inner: {
				string: "foo"
			}
		};

		const { code } = useTermsHydration( source );

		expect( code.constructor ).not.toHaveProperty( "staticInjected" );
		expect( code ).not.toHaveProperty( "injected" );
		expect( code ).not.toHaveProperty( "INJECTED" );

		expect( errorLog ).toHaveLength( 0 );
	} );

	it( "delivers computed properties for terms found in provided source", () => {
		const { code } = useTermsHydration( {
			outer: "=!true",
			inner: {
				string: ["=uppercase('hello')"]
			}
		} );

		expect( isReactive( code ) ).toBeTruthy();
		expect( code ).toEqual( { outer: false, inner: { string: ["HELLO"] } } );

		expect( errorLog ).toHaveLength( 0 );
	} );

	it( "updates computed properties based on terms when updating reactive data space", () => {
		const { code, data } = useTermsHydration( {
			outer: "=string(major)",
			inner: {
				string: "hello {{uppercase(string(employee.firstName))}}"
			}
		} );

		expect( code ).toEqual( { outer: "", inner: { string: "hello " } } );

		data.major = 5;
		expect( code ).toEqual( { outer: "5", inner: { string: "hello " } } );

		data.major = 123;
		expect( code ).toEqual( { outer: "123", inner: { string: "hello " } } );

		data.employee = { firstName: "John" };
		expect( code ).toEqual( { outer: "123", inner: { string: "hello JOHN" } } );

		expect( errorLog ).toHaveLength( 0 );
	} );

	it( "supports sloppy naming on demand which requires properties of data space to be all lower case by default", () => {
		const { code, data } = useTermsHydration( {
			outer: "=string(maJor)",
			inner: {
				string: "hello {{EXclamATE(UPPERCASE(eMPloyEe.fiRStName))}}"
			}
		}, {
			strictNaming: false,
			functions: {
				exclamate: s => ( s == null ? "" : s + "!" ),
			},
		} );

		expect( code ).toEqual( { outer: "", inner: { string: "hello " } } );

		data.major = 5;
		expect( code ).toEqual( { outer: "5", inner: { string: "hello " } } );

		data.major = 123;
		expect( code ).toEqual( { outer: "123", inner: { string: "hello " } } );

		data.employee = { firstName: "John" };
		expect( code ).toEqual( { outer: "123", inner: { string: "hello " } } );

		data.employee = { firstname: "John" };
		expect( code ).toEqual( { outer: "123", inner: { string: "hello JOHN!" } } );

		expect( errorLog ).toHaveLength( 0 );
	} );

	it( "associates data addressed in terms with mixed-case properties of provided custom data space at term's compile time when sloppy naming is enabled", () => {
		const { code, data } = useTermsHydration( {
			outer: "=string(maJor)",
			inner: {
				string: "hello {{UPPERCASE(stRing(eMPloyEe.fiRStName))}}"
			}
		}, {
			strictNaming: false,
			data: {
				Major: "Tom",
				EMPLOYEE: {
					firstName: "John"
				}
			}
		} );

		expect( data ).toEqual( { Major: "Tom", EMPLOYEE: { firstName: "John" } } );
		expect( isReactive( data ) ).toBeTruthy();

		expect( code ).toEqual( { outer: "Tom", inner: { string: "hello JOHN" } } );

		data.Major = 5;
		expect( code ).toEqual( { outer: "5", inner: { string: "hello JOHN" } } );

		data.EMPLOYEE.firstName = "Jane";
		expect( code ).toEqual( { outer: "5", inner: { string: "hello JANE" } } );

		data.EMPLOYEE = { firstName: "John" };
		expect( code ).toEqual( { outer: "5", inner: { string: "hello JOHN" } } );

		// gets ignored as it isn't changing part of data space that has been associated with term at its compile time
		data.employee = { firstName: "Jane" };
		expect( code ).toEqual( { outer: "5", inner: { string: "hello JOHN" } } );

		// is replacing container of property term has been associated with, but lacks property with same name as found there at compile time
		data.EMPLOYEE = { firstname: "Jane" };
		expect( code ).toEqual( { outer: "5", inner: { string: "hello " } } );

		expect( errorLog ).toHaveLength( 0 );
	} );

	it( "supports relative naming in terms on accessing data to read", () => {
		const source = {
			personal: {
				fullName: "=lastName + ', ' + firstName + ' (' + employedAs + ')'",
				initials: "=substring(personal.firstName, 0, 1) + substring(personal.lastName, 0, 1)",
			},
			job: {
				initialSalary: "=item( salaries, 0 )",
				latestSalary: "=item( job.salaries, length( job.salaries ) - 1 )"
			}
		};

		const data = {
			personal: {
				firstName: "Jane",
				lastName: "Doe",
			},
			job: {
				salaries: [ 20000, 24000, 25000 ],
			},
			employedAs: "supporter"
		};

		const { code, data: reactive } = useTermsHydration( source, { relativeNames: true, data } );

		expect( code.personal.fullName ).toEqual( "Doe, Jane (supporter)" );
		expect( code.personal.initials ).toEqual( "JD" );
		expect( code.job.initialSalary ).toEqual( 20000 );
		expect( code.job.latestSalary ).toEqual( 25000 );

		reactive.personal.firstName = "Frank";
		reactive.job.salaries.unshift( 18000 );
		reactive.employedAs = "assistant";

		expect( code.personal.fullName ).toEqual( "Doe, Frank (assistant)" );
		expect( code.personal.initials ).toEqual( "FD" );
		expect( code.job.initialSalary ).toEqual( 18000 );
		expect( code.job.latestSalary ).toEqual( 25000 );

		expect( errorLog ).toHaveLength( 0 );
	} );

	it( "supports relative naming in terms on accessing data to read in sloppy naming mode, too", () => {
		const source = {
			personal: {
				fullName: "=lastName + ', ' + firstName + ' (' + employedAs + ')'",
				initials: "=substring(personal.firstName, 0, 1) + substring(personal.lastName, 0, 1)",
			},
			job: {
				initialSalary: "=item( salaries, 0 )",
				latestSalary: "=item( job.salaries, length( job.salaries ) - 1 )"
			}
		};

		const data = {
			personal: {
				firstname: "Jane",
				lastNAME: "Doe",
			},
			job: {
				salaries: [ 20000, 24000, 25000 ],
			},
			EMPLOYEDAS: "supporter"
		};

		const { code, data: reactive } = useTermsHydration( source, { relativeNames: true, data, strictNaming: false } );

		expect( code.personal.fullName ).toEqual( "Doe, Jane (supporter)" );
		expect( code.personal.initials ).toEqual( "JD" );
		expect( code.job.initialSalary ).toEqual( 20000 );
		expect( code.job.latestSalary ).toEqual( 25000 );

		reactive.personal.firstname = "Frank";
		reactive.job.salaries.unshift( 18000 );
		reactive.EMPLOYEDAS = "assistant";

		expect( code.personal.fullName ).toEqual( "Doe, Frank (assistant)" );
		expect( code.personal.initials ).toEqual( "FD" );
		expect( code.job.initialSalary ).toEqual( 18000 );
		expect( code.job.latestSalary ).toEqual( 25000 );

		expect( errorLog ).toHaveLength( 0 );
	} );

	it( "considers optional callback for reducing path name of hierarchy's level containing some data-accessing term to hydrate when supporting relative names", () => {
		// test scenario close to common use case of forms processor
		const source = {
			forms: [
				{
					name: "first",
					fields: [
						{
							name: "fieldA",
						},
						{
							name: "fieldB",
							value: "=fieldA",
						},
						{
							name: "fieldC",
							value: "=first.fieldA",
						},
					]
				}
			]
		};

		const data = { first: { fieldA: "" } };

		const contextReducer = context => {
			const [ root, formIndex ] = context;

			if ( root !== "forms" || !( Number( formIndex ) > -1 ) ) {
				return []; // prevent context-related names
			}

			const form = source.forms[Number( formIndex )];
			if ( !form ) {
				return []; // prevent context-related names
			}

			return [form.name]; // support names relative to current form
		};

		const { code, data: reactive } = useTermsHydration( source, { contextReducer, relativeNames: true, data } );

		expect( code.forms[0].fields[1].value ).toEqual( "" );
		expect( code.forms[0].fields[2].value ).toEqual( "" );

		reactive.first = { fieldA: "foo" };

		expect( code.forms[0].fields[1].value ).toEqual( "foo" );
		expect( code.forms[0].fields[2].value ).toEqual( "foo" );

		expect( errorLog ).toHaveLength( 0 );
	} );

	describe( "automatically unwraps elements in a reactive array which", () => {
		it( "is an array", () => {
			const { code } = useTermsHydration( {
				root: [ 3, "=rootA + rootB", 5 ],
				nested: {
					list: [ true, "=item( nested, 0 ) + item( nested, 1 )", false ],
				},
			}, {
				data: {
					rootA: 2,
					rootB: 16,
					nested: [ 4, 6 ],
				}
			} );

			expect( Array.isArray( code.root ) ).toBe( true );
			expect( Array.isArray( code.nested.list ) ).toBe( true );
		} );

		it( "can be enumerated", () => {
			const { code } = useTermsHydration( {
				root: [ 3, "=rootA + rootB", 5 ],
				nested: {
					list: [ 12, "=item( nested, 0 ) + item( nested, 1 )", 3 ],
				},
			}, {
				data: {
					rootA: 2,
					rootB: 16,
					nested: [ 4, 6 ],
				}
			} );

			expect( code.root.reduce( ( s, v ) => s + v, 0 ) ).toBe( 26 );
			expect( code.nested.list.reduce( ( s, v ) => s + v, 0 ) ).toBe( 25 );
		} );

		it( "can be consumed in a computed consuming its elements", () => {
			const { code, data } = useTermsHydration( {
				root: [ 3, "=rootA + rootB", 5 ],
				nested: {
					list: [ true, "=item( nested, 0 ) + item( nested, 1 )", false ],
				},
			}, {
				data: {
					rootA: 2,
					rootB: 16,
					nested: [ 4, 6 ],
				}
			} );

			const outerLog = [];
			const innerLog = [];

			const outer = computed( () => code.root[1] > 20, { onTrigger: v => outerLog.push( v ) } );
			const inner = computed( () => code.nested.list[1] > 20, { onTrigger: v => innerLog.push( v ) } );

			expect( outerLog ).toHaveLength( 0 );
			expect( innerLog ).toHaveLength( 0 );
			expect( outer.value ).toBeFalsy();
			expect( inner.value ).toBeFalsy();

			data.rootA = 5;

			expect( outerLog ).toHaveLength( 1 );
			expect( innerLog ).toHaveLength( 0 );
			expect( outer.value ).toBeTruthy();
			expect( inner.value ).toBeFalsy();

			data.nested[1] = 20;

			expect( outerLog ).toHaveLength( 1 );
			expect( innerLog ).toHaveLength( 1 );
			expect( outer.value ).toBeTruthy();
			expect( inner.value ).toBeTruthy();

			data.nested = [ 3, 4, 5 ];

			expect( outerLog ).toHaveLength( 1 );
			expect( innerLog ).toHaveLength( 2 );
			expect( outer.value ).toBeTruthy();
			expect( inner.value ).toBeFalsy();
		} );

		it( "can be watched with watchEffect()", async() => {
			const { code, data } = useTermsHydration( {
				root: [ 3, "=rootA + rootB", 5 ],
				nested: {
					list: [ true, "=item( nested, 0 ) + item( nested, 1 )", false ],
				},
			}, {
				data: {
					rootA: 2,
					rootB: 16,
					nested: [ 4, 6 ],
				}
			} );

			const outerLog = [];
			const innerLog = [];

			watchEffect( () => outerLog.push( code.root[1] ) );
			watchEffect( () => innerLog.push( code.nested.list[1] ) );

			expect( outerLog ).toHaveLength( 1 );
			expect( innerLog ).toHaveLength( 1 );

			data.rootA = 5;
			await nextTick();

			expect( outerLog ).toHaveLength( 2 );
			expect( innerLog ).toHaveLength( 1 );

			data.nested[1] = 20;
			await nextTick();

			expect( outerLog ).toHaveLength( 2 );
			expect( innerLog ).toHaveLength( 2 );

			data.nested = [ 3, 4, 5 ];
			await nextTick();

			expect( outerLog ).toHaveLength( 2 );
			expect( innerLog ).toHaveLength( 3 );
		} );

		it( "supports extraction of refs e.g. to be watched", async() => {
			const { code, data } = useTermsHydration( {
				root: [ 3, "=rootA + rootB", 5 ],
				nested: {
					list: [ true, "=item( nested, 0 ) + item( nested, 1 )", false ],
				},
			}, {
				data: {
					rootA: 2,
					rootB: 16,
					nested: [ 4, 6 ],
				}
			} );

			const outerLog = [];
			const innerLog = [];
			const outer = toRef( () => code.root[1] );
			const inner = toRef( () => code.nested.list[1] );

			watch( outer, v => outerLog.push( v ) );
			watch( inner, v => innerLog.push( v ) );

			expect( outerLog ).toHaveLength( 0 );
			expect( innerLog ).toHaveLength( 0 );

			data.rootA = 5;
			await nextTick();

			expect( outerLog ).toHaveLength( 1 );
			expect( innerLog ).toHaveLength( 0 );

			data.nested[1] = 20;
			await nextTick();

			expect( outerLog ).toHaveLength( 1 );
			expect( innerLog ).toHaveLength( 1 );

			data.nested = [ 3, 4, 5 ];
			await nextTick();

			expect( outerLog ).toHaveLength( 1 );
			expect( innerLog ).toHaveLength( 2 );
		} );
	} );
} );
