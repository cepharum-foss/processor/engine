import { afterEach, beforeEach, describe, expect, expectTypeOf, it, vi } from "vitest";
import { useDefinition } from "@/composables/definition.js";
import { useReactiveAsPromise } from "@cepharum/vue3-utils/dist/vue3-utils.js";

describe( "Definition composable", () => {
	const errorLog = [];

	beforeEach( () => {
		errorLog.splice( 0 );

		vi.spyOn( console, "error" ).mockImplementation( ( ...args ) => {
			errorLog.push( args );
		} );
	} );

	afterEach( () => {
		vi.restoreAllMocks();
	} );

	describe( "provides useDefinition() for preparing a definition of forms to process which", () => {
		beforeEach( () => {
			errorLog.splice( 0 );
		} );

		it( "is a function", () => {
			expectTypeOf( useDefinition ).toBeFunction();
		} );

		it( "requires provision of a valid definition", async() => {
			const run = definition => useReactiveAsPromise( useDefinition( definition ), r => {
				if ( r.data && r.state ) {
					return true;
				}

				if ( r.error ) {
					throw r.error;
				}

				return false;
			} );

			await expect( run() ).rejects.toThrow();
			expect( errorLog ).toHaveLength( 1 );

			await expect( run( {} ) ).rejects.toThrow();
			expect( errorLog ).toHaveLength( 2 );

			await expect( run( { forms: true } ) ).rejects.toThrow();
			expect( errorLog ).toHaveLength( 3 );

			await expect( run( { forms: {} } ) ).rejects.toThrow();
			expect( errorLog ).toHaveLength( 4 );

			await expect( run( { forms: [] } ) ).rejects.toThrow();
			expect( errorLog ).toHaveLength( 5 );

			await expect( run( { forms: [{}] } ) ).rejects.toThrow();
			expect( errorLog ).toHaveLength( 6 );

			await expect( run( { forms: [{ name: 123 }] } ) ).rejects.toThrow();
			expect( errorLog ).toHaveLength( 7 );

			await expect( run( { forms: [{ name: "123" }] } ) ).rejects.toThrow();
			expect( errorLog ).toHaveLength( 8 );

			await expect( run( { forms: [{ name: "f123" }] } ) ).rejects.toThrow();
			expect( errorLog ).toHaveLength( 9 );

			await expect( run( { forms: [{ name: "f123", fields: 1 }] } ) ).rejects.toThrow();
			expect( errorLog ).toHaveLength( 10 );

			await expect( run( { forms: [{ name: "f123", fields: [] }] } ) ).rejects.toThrow();
			expect( errorLog ).toHaveLength( 11 );

			await expect( run( { forms: [{ name: "f123", fields: [{}] }] } ) ).rejects.toThrow();
			expect( errorLog ).toHaveLength( 12 );

			await expect( run( { forms: [{ name: "f123", fields: [{ name: 123 }] }] } ) ).rejects.toThrow();
			expect( errorLog ).toHaveLength( 13 );

			await expect( run( { forms: [{ name: "f123", fields: [{ name: "123" }] }] } ) ).rejects.toThrow();
			expect( errorLog ).toHaveLength( 14 );

			await expect( run( { forms: [{ name: "f123", fields: [{ name: "f123" }] }] } ) ).rejects.toThrow();
			expect( errorLog ).toHaveLength( 15 );

			await expect( run( { forms: [{ name: "f123", fields: [{ name: "f123" }] }], processors: 1 } ) ).rejects.toThrow();
			expect( errorLog ).toHaveLength( 16 );

			await expect( run( { forms: [{ name: "f123", fields: [{ name: "f123" }] }], processors: [] } ) ).rejects.toThrow();
			expect( errorLog ).toHaveLength( 17 );

			await expect( run( { forms: [{ name: "f123", fields: [{ name: "f123" }] }], processors: [{}] } ) ).rejects.toThrow();
			expect( errorLog ).toHaveLength( 18 );

			await expect( run( { forms: [{ name: "f123", fields: [{ name: "f123" }] }], processors: [{ type: "foo" }] } ) ).resolves.not.toThrow();
			expect( errorLog ).toHaveLength( 18 );
		} );

		it( "caches prepared definition by provided source", () => {
			const definition = { forms: [{ name: "f123", fields: [{ name: "f123" }] }], processors: [{ type: "foo" }] };

			const a = useDefinition( definition );
			const b = useDefinition( definition );

			expect( a ).toBe( b );
			expect( a.code ).toBe( b.code );
			expect( a.data ).toBe( b.data );
			expect( a.state ).toBe( b.state );
		} );

		it( "provides cached result on providing result of a previous run based on a source given as object", () => {
			const definition = { forms: [{ name: "f123", fields: [{ name: "f123" }] }], processors: [{ type: "foo" }] };

			const a = useDefinition( definition );
			const b = useDefinition( a.code );

			expect( a ).toBe( b );
			expect( a.code ).toBe( b.code );
			expect( a.data ).toBe( b.data );
			expect( a.state ).toBe( b.state );
		} );

		it( "accepts YAML-encoded definition as string", () => {
			expect( () => useDefinition( `forms:\n  - name: f123\n    fields:\n      - name: f123\nprocessors:\n  - type: foo\n` ) ).not.toThrow();

			expect( errorLog ).toHaveLength( 0 );
		} );

		it( "caches prepared definition by provided YAML-encoded source", () => {
			const definition = `forms:\n  - name: f123\n    fields:\n      - name: f123\nprocessors:\n  - type: foo\n`;

			const a = useDefinition( definition );
			const b = useDefinition( definition );

			expect( a ).toBe( b );
			expect( a.code ).toBe( b.code );
			expect( a.data ).toBe( b.data );
			expect( a.state ).toBe( b.state );
		} );

		it( "provides cached result on providing result of a previous run based on a source given as YAML-encoded string", () => {
			const definition = `forms:\n  - name: f123\n    fields:\n      - name: f123\nprocessors:\n  - type: foo\n`;

			const a = useDefinition( definition );
			const b = useDefinition( a.code );

			expect( a ).toBe( b );
			expect( a.code ).toBe( b.code );
			expect( a.data ).toBe( b.data );
			expect( a.state ).toBe( b.state );
		} );

		it( "accepts valid definition delivered by a provided callback", () => {
			expect( () => useDefinition( () => ( { forms: [{ name: "f123", fields: [{ name: "f123" }] }], processors: [{ type: "foo" }] } ) ) ).not.toThrow();

			expect( errorLog ).toHaveLength( 0 );
		} );

		it( "creates separate definitions on providing callback for delivering valid definition", () => {
			const definition = { forms: [{ name: "f123", fields: [{ name: "f123" }] }], processors: [{ type: "foo" }] };
			const fn = () => definition;

			const a = useDefinition( fn );
			const b = useDefinition( fn );

			expect( a ).not.toBe( b );
			expect( a.code ).not.toBe( b.code );
			expect( a.data ).not.toBe( b.data );
			expect( a.state ).not.toBe( b.state );
		} );

		it( "provides cached result on providing result of a previous run based on a source given as callback delivering actual definition", () => {
			const definition = { forms: [{ name: "f123", fields: [{ name: "f123" }] }], processors: [{ type: "foo" }] };

			const a = useDefinition( () => definition );
			const b = useDefinition( a.code );

			expect( a ).toBe( b );
			expect( a.code ).toBe( b.code );
			expect( a.data ).toBe( b.data );
			expect( a.state ).toBe( b.state );
		} );

		it( "accepts valid definition as YAML-encoded string delivered by a provided callback", () => {
			expect( () => useDefinition( () => `forms:\n  - name: f123\n    fields:\n      - name: f123\nprocessors:\n  - type: foo\n` ) ).not.toThrow();

			expect( errorLog ).toHaveLength( 0 );
		} );

		it( "creates separate definitions on providing callback for delivering valid definition as YAML-encoded string", () => {
			const definition = `forms:\n  - name: f123\n    fields:\n      - name: f123\nprocessors:\n  - type: foo\n`;
			const fn = () => definition;

			const a = useDefinition( fn );
			const b = useDefinition( fn );

			expect( a ).not.toBe( b );
			expect( a.code ).not.toBe( b.code );
			expect( a.data ).not.toBe( b.data );
			expect( a.state ).not.toBe( b.state );
		} );

		it( "provides cached result on providing result of a previous run based on a source given as callback delivering actual definition as YAML-encoded string", () => {
			const definition = `forms:\n  - name: f123\n    fields:\n      - name: f123\nprocessors:\n  - type: foo\n`;

			const a = useDefinition( () => definition );
			const b = useDefinition( a.code );

			expect( a ).toBe( b );
			expect( a.code ).toBe( b.code );
			expect( a.data ).toBe( b.data );
			expect( a.state ).toBe( b.state );
		} );
	} );
} );
