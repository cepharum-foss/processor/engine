import { parse } from "yaml";
import { error } from "./log.js";
import { useTermsHydration } from "./terms-hydration.js";
import { computed, reactive, readonly } from "vue";

const CacheReference = Symbol( "CacheReference" );

const cache = new WeakMap();
const scalarCache = new Map();

const ptnValidName = /^[_\p{L}][_\p{L}0-9]*$/u;

/**
 * Provides a readonly normalized reactive definition of forms to process.
 *
 * @param {object|function():object|string} source original definition or callback to invoke for delivering that definition
 * @returns {ReactiveDefinitionWithData} reactive definition of forms and their prepared reactive data space
 */
export function useDefinition( source ) {
	/*
	 * check cache for existing record
	 */

	// check if provided source is result of a previous run and look up the
	// cache record ID assigned back then
	if ( source?.[CacheReference] ) {
		return cache.get( source[CacheReference] );
	}

	if ( source?.code?.[CacheReference] ) {
		return cache.get( source.code[CacheReference] );
	}

	// check if provided source itself has been used as cache record ID before
	if ( source ) {
		switch ( typeof source ) {
			case "object" :
				if ( cache.has( source ) ) {
					return cache.get( source );
				}
				break;

			case "function" :
				break;

			default :
				if ( scalarCache.has( source ) ) {
					const key = scalarCache.get( source );

					if ( cache.has( key ) ) {
						return cache.get( key );
					}
				}
		}
	}

	/*
	 * fetch definition in case a callback has been provided
	 */
	let definition = source;

	if ( typeof definition === "function" ) {
		definition = definition();
	}

	// instantly create result descriptor with an empty description (required to
	// prevent race conditions causing duplicate processing of same source)
	const result = reactive( { code: {} } );

	/*
	 * track result in cache
	 */
	const cacheId = Symbol();

	// mark temporary code with CacheReference so that preliminary requests for
	// processing same definition hit the cache even on deferred definition retrievals
	Object.defineProperty( result.code, CacheReference, { value: cacheId } );

	cache.set( cacheId, result );

	// track result by provided source itself, too
	if ( typeof source === "string" ) {
		// strings don't work as keys in a WeakMap
		// -> use separate Map assigning used cache record ID to provided string
		scalarCache.set( source, cacheId );
	} else if ( typeof source === "object" ) {
		cache.set( source, result );
	}

	/*
	 * trigger eventual processing of definition as soon as it is available
	 */
	if ( definition instanceof Promise ) {
		definition.then( processDefinition ).catch( cause => { result.error = cause; } );
	} else {
		try {
			processDefinition( definition );
		} catch ( cause ) {
			result.error = cause;
		}
	}

	return result;

	/**
	 * Processes eventually available definition of forms to process.
	 *
	 * @param {Definition} fetchedSource definition of forms to process
	 * @returns {void}
	 */
	function processDefinition( fetchedSource ) {
		if ( typeof fetchedSource === "string" ) {
			fetchedSource = parse( fetchedSource );
		}

		if ( fetchedSource && typeof fetchedSource === "object" ) {
			if ( typeof window === "undefined" ) {
				fetchedSource = JSON.parse( JSON.stringify( fetchedSource ) );
			} else {
				fetchedSource = window.structuredClone( fetchedSource );
			}
		}

		normalizeFormsDefinition( fetchedSource );

		/*
		 * compile normalized definition into set of reactive definition with
		 * hydrated terms, related data space and state descriptors
		 */
		const dataSpace = createDataSpace( fetchedSource );
		const state = createState( fetchedSource );

		// expose state descriptors readonly in data space
		dataSpace.$state = readonly( state );
		dataSpace.$definition = fetchedSource; // preliminary injection of definition so that name qualification is working

		const { code, data } = useTermsHydration( fetchedSource, {
			data: dataSpace,
			strictNaming: false,
			relativeNames: true,
			codeLocale: fetchedSource.lang || "en",
			// maps path name of a term in provided definition into related scope of
			// that term in prepared data space for relative naming support
			contextReducer( context ) {
				const scope = [];
				let iter = code;
				let name = "forms";

				for ( let i = 0; iter && i < context.length; i += 2 ) {
					const property = context[i];
					const index = Number( context[i + 1] );

					if ( property !== name || !( index > -1 ) ) {
						break;
					}

					iter = iter.fields[index];
					name = "fields";

					scope.push( iter.name );
				}

				return scope;
			}
		} );

		// transfer cache reference put on result temporarily before
		Object.defineProperty( code, CacheReference, { value: result.code[CacheReference] } );

		// replace preliminary injection of original definition into its data space
		// with readonly reactive version of hydrated definition
		data.$definition = readonly( code );

		// put code, data and state into record prepared before for caching
		result.code = data.$definition;
		result.data = data;
		result.state = state;
	}
}

/**
 * Validates and normalizes provided name.
 *
 * Names are strings consisting of letters, underscores and digits, only.
 *
 * @param {any} name some name to validate and normalize
 * @returns {string} normalized valid name
 */
export function normalizeName( name ) {
	if ( typeof name === "string" ) {
		name = name.trim();

		if ( !ptnValidName.test( name ) ) {
			throw error( new TypeError( `invalid name: ${name}` ) );
		}

		return name.trim();
	}

	throw error( new TypeError( `invalid type of name: ${name} (${typeof name})` ) );
}

/**
 * Validates and adjusts provided definition of forms to basically comply with
 * expected structure.
 *
 * @param {object} source definition of forms to validate and normalize, normalized valid definition on return
 * @returns {void}
 */
export function normalizeFormsDefinition( source ) {
	if ( !source || typeof source !== "object" ) {
		throw error( new TypeError( "missing or invalid definition of forms to process" ) );
	}

	if ( !Array.isArray( source.forms ) || !source.forms.length ) {
		throw error( new TypeError( "missing list of forms to process" ) );
	}

	const codeLocale = source.lang || "en";

	for ( const form of source.forms ) {
		form.name = normalizeName( form.name );

		normalizeFields( form, "form", codeLocale );
	}

	for ( const form of source.forms ) {
		if ( source.forms.find( f => f !== form && f.name.toLocaleLowerCase( codeLocale ) === form.name.toLocaleLowerCase( codeLocale ) ) ) {
			throw error( new TypeError( `duplicate form name ${form.name}` ) );
		}
	}

	if ( !Array.isArray( source.processors ) || !source.processors.length ) {
		throw error( new TypeError( "missing list of data processors" ) );
	}

	for ( const processor of source.processors ) {
		normalizeProcessor( processor );
	}
}

/**
 * Recursively validates and adjusts definition of fields in provided context.
 *
 * @param {object} context some definition (e.g. of a form or a single field) containing a subset of fields to normalize
 * @param {string} contextName type name of context, used in error messages
 * @param {string} codeLocale locale of definition/code (which is: the locale e.g. used to name forms and fields, not the locale a user is going to see)
 * @returns {void}
 */
export function normalizeFields( context, contextName, codeLocale ) {
	if ( !Array.isArray( context.fields ) ) {
		throw error( new TypeError( `missing or invalid list of fields in ${contextName} named ${context.name}` ) );
	}

	for ( const field of context.fields ) {
		field.name = normalizeName( field.name );

		if ( !field.type ) {
			field.type = "text";
		}

		if ( Array.isArray( field.fields ) ) {
			normalizeFields( field, "field", codeLocale );
		}
	}

	for ( const field of context.fields ) {
		if ( context.fields.find( f => f !== field && f.name.toLocaleLowerCase( codeLocale ) === field.name.toLocaleLowerCase( codeLocale ) ) ) {
			throw error( new TypeError( `duplicate field name ${field.name} in ${contextName} name ${context.name}` ) );
		}
	}
}

/**
 * Validates and adjusts definition of a single data processor.
 *
 * @param {object} definition definition of single processor e.g. invoke after data has been gathered through forms
 * @returns {void}
 */
export function normalizeProcessor( definition ) {
	if ( !definition || typeof definition !== "object" ) {
		throw error( new TypeError( `missing or invalid definition of a processor` ) );
	}

	if ( !definition.type ) {
		if ( definition.url ) {
			definition.type = "send";
		}

		if ( !definition.type ) {
			throw error( new TypeError( `missing type of processor` ) );
		}
	}
}


/**
 * Creates a data space for a normalized valid definition of forms.
 *
 * @param {Definition} definition definition of forms
 * @returns {object} data space for provided definition of forms
 */
export function createDataSpace( definition ) {
	const data = {};

	for ( const form of definition.forms ) {
		data[form.name] = createFields( {}, form.fields );
	}

	return data;

	/**
	 * Recursively creates data space for defined set of named fields.
	 *
	 * @param {object} target object to populate with properties per defined field
	 * @param {FieldDefinition[]} source list of field definitions
	 * @returns {object} provided target with properties created for every listed field
	 */
	function createFields( target, source ) {
		for ( const field of source ) {
			const fieldName = field.name;

			if ( Array.isArray( field.fields ) ) {
				target[fieldName] = createFields( {}, field.fields );
			} else {
				target[fieldName] = undefined;
			}
		}

		return target;
	}
}

/**
 * Creates a state descriptor for every defined form and either form's fields.
 *
 * @param {Definition} definition definition of forms
 * @returns {FormsState} map of either form's name into its state descriptor
 */
export function createState( definition ) {
	const state = reactive( {} );

	for ( const form of definition.forms ) {
		const formName = form.name;

		state[formName] = {
			touched: false,
			fields: createFieldsState( reactive( {} ), form.fields ),
		};
	}

	return reactive( state );

	/**
	 * Recursively creates state descriptor for every field in provided context.
	 *
	 * @param {Object<string, FieldState>} states maps names of fields in provided context into either field's state descriptor
	 * @param {FieldDefinition[]} source list of field definitions
	 * @returns {Object<string, FieldState>} provided set of state descriptors per defined field
	 */
	function createFieldsState( states, source ) {
		for ( const field of source ) {
			const fieldName = field.name;
			const fieldState = states[fieldName] = reactive( {
				touched: false,
				changed: false,
			} );

			if ( Array.isArray( field.fields ) ) {
				fieldState.fields = createFieldsState( reactive( {} ), field.fields );

				fieldState.valid = computed( () => {
					for ( const { valid } of Object.values( fieldState.fields ) ) {
						if ( !valid ) {
							return false;
						}
					}

					return true;
				} );

				fieldState.touched = computed( () => {
					for ( const { touched } of Object.values( fieldState.fields ) ) {
						if ( touched ) {
							return true;
						}
					}

					return false;
				} );

				fieldState.changed = computed( () => {
					for ( const { changed } of Object.values( fieldState.fields ) ) {
						if ( changed ) {
							return true;
						}
					}

					return false;
				} );
			} else {
				fieldState.errors = [];

				fieldState.touched = false;
				fieldState.changed = false;
				fieldState.valid = computed( () => fieldState.errors.length === 0 );
			}
		}

		return states;
	}
}
