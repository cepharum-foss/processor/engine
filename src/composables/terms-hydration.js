import { computed, reactive, readonly, unref } from "vue";
import { error } from "@/composables/log.js";
import { Compiler, Functions, Interpolation } from "simple-terms";

const termsCache = new Map();

/**
 * Adds auto-unwrapping computed references in reactive arrays as it isn't
 * supported by Vue3 natively.
 *
 * @type {{get(Array, string|number|Symbol): (unknown)}}
 */
const ArrayUnwrapper = {
	get( target, property ) {
		switch ( typeof property ) {
			case "string" :
			case "number" :
				if ( /^[-+]?\d+$/.test( property ) ) {
					return unref( target[Number( property )] );
				}

				// falls through
			default :
				return target[property];
		}
	}
};

/**
 * Converts a provided hierarchy of data into a reactive version with optionally
 * contained terms being computed against some optionally provided data space.
 *
 * That data space should be reactive for the terms to be as effective as
 * possible.
 *
 * @template T
 * @param {T} source a hierarchy of data potentially containing terms to hydrate
 * @param {TermsHydrationOptions} options options for converting and for evaluating terms
 * @returns {DataWithHydratedTerms<T>} the resulting reactive hierarchy of data and either provided or some created data space used on evaluating found terms
 */
export function useTermsHydration( source, options = {} ) {
	if ( !source || typeof source !== "object" || Array.isArray( source ) ) {
		throw error( "provided source is not a hierarchy of data" );
	}

	const strictNames = options.strictNaming ?? true;
	const unwrapArrays = options.unwrapArrays !== false;
	const data = reactive( options.data || {} );
	const functions = options.functions ? { ...Functions, ...options.functions } : Functions;
	let code = reactive( convertLevel(
		{},
		source,
		data,
		functions,
		nameQualifier,
		options.termsCache || ( options.relativeNames || ( options.data && !strictNames ) ? null : termsCache ),
		strictNames,
		unwrapArrays
	) );

	if ( options.readonly === true || ( options.readonly == null && unwrapArrays ) ) {
		code = readonly( code );
	}

	return {
		code,
		data,
	};

	/**
	 * Qualifies at a term's compile time the segments of a data source it is
	 * looking up in data space.
	 *
	 * @note The data space isn't provided as part of qualification as per
	 *       simple-terms engine. Qualification is still looking into the data
	 *       space provided in containing function. Thus, qualification works
	 *       with a properly prepared data space, only.
	 *
	 * @param {string[]} segments segments of data source
	 * @param {string[]} context segments of path into hierarchy of data where the compiled term has been found in
	 * @returns {string[]} segments to eventually use for looking up some data
	 */
	function nameQualifier( segments, context ) {
		const {
			contextReducer,
			relativeNames,
			codeLocale,
			strictNaming,
		} = options;

		const prefix = relativeNames ? contextReducer ? contextReducer( context ) : context : [];

		const candidates = prefix?.length > 0 ? [
			[ ...prefix, ...segments ],
			segments,
		] : [segments];

		nextCandidate: for ( const candidate of candidates ) { // eslint-disable-line no-labels
			const path = [];
			let iter = data;

			nextSegment: for ( const segment of candidate ) { // eslint-disable-line no-labels
				if ( iter.hasOwnProperty( segment ) ) {
					// segment matches case-sensitively
					path.push( segment );
					iter = iter[segment];
					continue;
				}

				if ( !( strictNaming ?? true ) ) {
					// names are accepted to use different case
					// -> try matching case-insensitively, stick to first match
					const lookup = segment.toLocaleLowerCase( codeLocale || "en" );

					for ( const [ key, value ] of Object.entries( iter ) ) {
						if ( key.toLocaleLowerCase( codeLocale || "en" ) === lookup ) {
							path.push( key );
							iter = value;

							continue nextSegment; // eslint-disable-line no-labels
						}
					}
				}

				continue nextCandidate; // eslint-disable-line no-labels
			}

			return path;
		}

		// out of qualification candidates -> stick to original path
		return segments;
	}
}

/**
 * Recursively hydrates terms found in property values of current hierarchy
 * level.
 *
 * @template {object} T
 * @param {T} dest resulting deep copy of source with computed values instead of strings containing terms
 * @param {T} source single level of originally provided hierarchy to search for terms to hydrate
 * @param {object} data reactive data space on which all terms will be processed later
 * @param {Object<string, function>} functions pool of functions to support in terms
 * @param {function(string[], string[]): string[]} nameQualifier callback qualifying names of data space properties used in terms at their compile time
 * @param {Map<string,CompiledTerm>?} cache a custom global cache of previously compiled terms to use instead of per-level cache
 * @param {boolean?} strictNaming if true, names addressing properties of data space are kept as-is for strict naming
 * @param {boolean?} unwrapArrays if false, array items aren't unwrapped automatically
 * @returns {object} destination provided in `dest`
 */
function convertLevel( dest, source, data, functions, nameQualifier, cache, strictNaming, unwrapArrays ) {
	const localTermsCache = cache || new Map();

	for ( let key of Object.keys( source ) ) {
		if ( Array.isArray( source ) ) {
			key = Number( key );
		}

		const value = source[key];

		switch ( key ) {
			case "constructor" :
			case "prototype" :
			case "__proto__" :
				break;

			default :
				switch ( typeof value ) {
					case "object" : {
						const qualifier = ( segments, context ) => nameQualifier( segments, [ key, ...context ] );

						if ( unwrapArrays && Array.isArray( value ) ) {
							dest[key] = new Proxy( convertLevel( [], value, data, functions, qualifier, cache, strictNaming, unwrapArrays ), ArrayUnwrapper );
						} else if ( value ) {
							dest[key] = convertLevel( {}, value, data, functions, qualifier, cache, strictNaming, unwrapArrays );
						}
						break;
					}

					case "string" : {
						const trimmed = value.trim();
						const qualifier = segments => nameQualifier( segments, [] );

						if ( trimmed[0] === "=" ) {
							// all but leading "=" is a term to process on current data space, may yield any type of value
							const term = Compiler.compile( trimmed.slice( 1 ), functions, localTermsCache, qualifier, strictNaming );

							dest[key] = computed( () => term( data ) );
						} else {
							// interpolate terms wrapped in {{ }} based on current data space, always yields a string
							const term = Interpolation.parse( trimmed, functions, {
								asFunction: true,
								keepLiteralString: true,
								qualifier,
								cache: localTermsCache,
								strictNaming,
							} );

							dest[key] = typeof term === "function" ? computed( () => term( data ) ) : term;
						}
						break;
					}

					default :
						dest[key] = value;
						break;
				}
		}
	}

	return dest;
}
