/**
 * Logs message with prefix clearly associating it with current library to
 * console.
 *
 * @param {string} message message to log
 * @param {any[]} args additional arguments to message, e.g. for interpolating placeholders in message
 * @returns {void}
 */
export function log( message, ...args ) {
	console.log( "[forms-processor]", message, ...args );
}

/**
 * Logs warning message with prefix clearly associating it with current library
 * to console.
 *
 * @param {string} message message to log
 * @param {any[]} args additional arguments to message, e.g. for interpolating placeholders in message
 * @returns {void}
 */
export function warn( message, ...args ) {
	console.warn( "[forms-processor]", message, ...args );
}

/**
 * Logs message as error to browser console.
 *
 * @param {T} message message to log
 * @param {any[]} args list of additional arguments
 * @return {T} first argument provided by caller
 * @template T string or Error
 */
export function error( message, ...args ) {
	console.error( "[forms-processor]", message, ...args );

	return message instanceof Error ? message : typeof message === "string" ? new Error( message ) : message;
}
