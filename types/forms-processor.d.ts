import {UnwrapNestedRefs} from "vue";

declare namespace FormsProcessor {
	/**
	 * Combines two reactive hierarchies of data with one of them (`code`)
	 * optionally computing some of its elements via terms that are processed
	 * in context of the other hierarchy (`data`).
	 */
	export interface DataWithHydratedTerms<T = object> {
		/**
		 * Provides a hierarchy of data optionally containing some properties
		 * that get computed in context of separately provided data space.
		 */
		code: UnwrapNestedRefs<T>;

		/**
		 * Provides a hierarchy of data used in computing elements of `code`
		 * based on some terms.
		 */
		data: UnwrapNestedRefs<object>;
	}

	/**
	 * Describes options supported for hydrating terms in a hierarchy of data.
	 */
	export interface TermsHydrationOptions {
		/**
		 * Provides a custom data space to use on processing terms.
		 *
		 * @note Qualifying names at compile time does not work without
		 *       providing a well-prepared non-empty custom data space here. If
		 *       you intend to support sloppy naming in combination with a data
		 *       space consisting of property names with uppercase letters, you
		 *       MUST provide a custom data space
		 *
		 * @note On providing a custom data space here while disabling strict
		 *       naming, the implicit terms caching is limited to single levels
		 *       of hydrated hierarchy. Sloppy names are associated with actual
		 *       property names found in a provided data space.
		 *       If you hydrate a single hierarchy multiple times or separate
		 *       hierarchies, each time with a different custom data space,
		 *       global caching may prevent compile-time name qualification.
		 */
		data?: object | UnwrapNestedRefs<object>;

		/**
		 * Provides a custom map of functions to use on processing terms.
		 *
		 * @note To support fault-tolerance in terms naming functions, names of
		 *       all functions given here must not contain any uppercase letter.
		 */
		functions?: { [key: string]: Function };

		/**
		 * If true, terms have to name elements of data space case-sensitively.
		 * Otherwise, names are matched case-insensitively against custom data
		 * space during terms hydration (you need to provide a well-prepared
		 * custom data space for that to work).
		 */
		strictNaming?: boolean;

		/**
		 * Optionally replaces segments of path name leading to the level of
		 * processed hierarchy that is containing term using a name to be
		 * qualified.
		 *
		 * Use this callback in case hydrated hierarchy has a different yet
		 * related structure than the provided data space.
		 */
		contextReducer?: (context: string[] ) => string[];

		/**
		 * Controls name qualification to support relative names based on a
		 * term's position in hierarchy of potential terms to hydrate.
		 *
		 * @note On enabling relative names, the implicit terms caching is
		 *       limited to single levels of hydrated hierarchy.
		 *       When supporting relative names, global caching may prevent
		 *       context-based qualification of names at compile term.
		 */
		relativeNames?: boolean;

		/**
		 * Provides a custom cache for globally caching compiled terms.
		 *
		 * This is used in preference over any implicit terms caching, even with
		 * enabled relative names support or on providing custom data space.
		 */
		termsCache?: Map<string, function>;

		/**
		 * Selects custom locale to assume for keys in data space.
		 *
		 * This is affecting the locale-aware case-insensitive matching of names
		 * during name qualification.
		 */
		codeLocale?: string;

		/**
		 * Controls whether items of arrays are unwrapped automatically or not.
		 *
		 * By default, items of arrays get unwrapped as a convenience extension
		 * to what Vue3 offers. However, this causes resulting hierarchy with
		 * hydrated terms to cause reactivity issues when adjusting arrays it is
		 * containing e.g. through methods like unshift() or pop().
		 *
		 * Consider setting this option to `false` if you intend to adjust the
		 * returned hierarchy.
		 */
		unwrapArrays?: boolean;

		/**
		 * If true, the resulting hierarchy is marked as read only.
		 *
		 * Defaults to true if the hierarchy has unwrapped arrays, too.
		 * Otherwise, it defaults to false. This is due to mutating unwrapped
		 * arrays in the result is breaking its reactivity. You need to
		 * explicitly set this option false to get a mutable result with
		 * unwrapped arrays.
		 */
		readonly?: boolean;
	}

    /**
     * Defines common properties of any control's current state.
     */
    export interface StateBasics {
        /**
         * Indicates if the control associated with this state has been focused
         * by the user at least once.
         */
        touched?: boolean;

        /**
         * Indicates whether the value of the control associated with this state
         * has changed or not.
         */
        adjusted?: boolean;
    }

    /**
     * Describes the state of a non-interactive compound control that consists
     * of one or more other controls.
     */
    export interface StateNode extends StateBasics {
        states: StateNodes;
    }

    /**
     * Describes the state of an interactive elemental control that can be part
     * of compound controls and forms.
     */
    export interface StateLeaf extends StateBasics {
        value: any;
    }

    /**
     * Defines a map of control names into either control's state. This is e.g.
     * used in compound controls.
     */
    export interface StateNodes {
        [key: string]: StateNode | StateLeaf;
    }

    /**
     * Defines a hierarchy of values.
     */
    export interface ValuesNode {
        [key: string]: any | ValuesNode
    }

    /**
     * Defines essential properties in a hierarchical definition for identifying
     * forms, fields and other controls.
     */
    export interface HierarchyNode {
        /**
         * Provides the name of current node in a hierarchy which is expected to
         * be unique in context of its siblings.
         */
        name: string;
    }

    /**
     * Defines essential properties at top level of a hierarchical definition.
     */
    export interface HierarchyOfForms {
        /**
         * Lists definitions of forms each uniquely identified by its name.
         */
        forms: HierarchyOfFields[];
    }

    /**
     * Defines essential properties of fields and subordinated compound controls
     * in a hierarchical definition.
     */
    export interface HierarchyOfFields extends HierarchyNode {
        fields: ( HierarchyOfFields | HierarchyNode )[];
    }

    /**
     * Describes a set of forms each consisting of fields and other controls and
     * a set of processors used to eventually process information gathered from
     * a user of defined forms.
     */
    export interface Definition {
        /**
         * Lists definition of forms to provide to a user for gathering
         * information.
         */
        forms: FormDefinition[];

        /**
         * Lists a sequence of processors executed sequentially on data gathered
         * from user via presented forms.
         */
        processors?: ProcessorDefinition[];
    }

	/**
	 * Describes runtime context of forms to process.
	 *
	 * @note Due to the support for caching deferred retrieval of definitions,
	 *       a processed definition may result in preliminary state where there
	 *       is no `data` and no `state` and just a proxy for the eventually
	 *       available `code`.
	 */
	export interface ReactiveDefinitionWithData {
		/**
		 * Reactively defines forms to process.
		 *
		 * @note This property holds a proxy solely for proper caching while
		 *       processing a deferred definition is pending. It becomes
		 *       available after processing has finished.
		 */
		code: Reactive<Definition>;

		/**
		 * Reactively holds data gathered through defined forms.
		 *
		 * @note This property is missing while processing a deferred definition
		 *       is pending. But it is mandatory for an operational definition.
		 */
		data?: Reactive<object>;

		/**
		 * Reactively tracks states of defined forms and their fields.
		 *
		 * @note This property is missing while processing a deferred definition
		 *       is pending. But it is mandatory for an operational definition.
		 */
		state?: Reactive<FormsState>;

		/**
		 * Indicates error on preparing a given definition of forms to process
		 * e.g. due to lacking mandatory information.
		 */
		error?: Error;
	}

    /**
     * Describes definition of a single form.
     */
    export interface FormDefinition {
        /**
         * Uniquely names the form.
         */
        name: string;

        /**
         * Lists definitions of fields (and basically any other control) the
         * form is consisting of.
         */
        fields: FieldDefinition[];
    }

    /**
     * Combines several more particular definitions of supported fields.
     */
    export type FieldDefinition =
        FieldInputDefinition |
        FieldTextDefinition |
        FieldNumberDefinition |
        FieldDateDefinition |
        FieldEmailDefinition |
        FieldStaticDefinition |
        FieldOptionsDefinition |
        FieldUploadDefinition;

    /**
     * Describes basic properties for defining any field or control in a form.
     */
    export interface FieldAbstractDefinition {
        /**
         * Provides a label to present next to either field or control.
         */
        label?: DisplayText;

        /**
         * Indicates if the field is currently visible or not.
         */
        visible?: boolean;
    }

    export interface FieldInteractiveDefinition extends FieldAbstractDefinition {
        name: string;
        default?: any;
        required?: boolean;
        disabled?: boolean;
        readonly?: boolean;
    }

    export interface FieldInputDefinition extends FieldInteractiveDefinition {
        type?: "text";
        multiline?: false;
        pattern: string;
        minCharacters?: number;
        maxCharacters?: number;
    }

    export interface FieldTextDefinition extends FieldInteractiveDefinition {
        type: "text";
        multiline: true;
        minCharacters?: number;
        maxCharacters?: number;
        minWords?: number;
        maxWords?: number;
    }

    export interface FieldNumberDefinition extends FieldInteractiveDefinition {
        type: "number";
        min?: number;
        max?: number;
        step?: number;
    }

    export interface FieldDateDefinition extends FieldInteractiveDefinition {
        type: "date" | "time" | "datetime";
        min?: string | Date;
        max?: string | Date;
    }

    export interface FieldEmailDefinition extends FieldInteractiveDefinition {
        type: "email" | "mail";
    }

    export interface FieldStaticDefinition extends FieldAbstractDefinition {
        type: "static";
        text: DisplayText;
        collapsed?: boolean;
        toggle: DisplayText;
    }

    export interface FieldOptionsDefinition extends FieldInteractiveDefinition {
        type: "select" | "multiselect" | "checkbox" | "radio";
        options: OptionDefinition[];
        maxOptions?: number;
    }

    export interface FieldUploadDefinition extends FieldInteractiveDefinition {
        type: "file" | "upload";
        accept?: string[];
        limit?: number;
    }

    export interface OptionDefinition {
        value: any;
        label: DisplayText;
    }

    export interface TextPerLocale {
        [key: string]: string;
    }

    export type DisplayText = string | TextPerLocale;

	export interface FormsState {
		[formName: string]: FormState;
	}

	export interface FormState {
		fields: { [fieldName: string]: FieldState };
		touched: boolean;
		changed: boolean;
		valid: boolean;
	}

	export interface FieldState {
		touched: boolean;
		changed: boolean;
		valid: boolean;
		errors: string[];
	}

    export type ProcessorDefinition = SendUrlProcessorDefinition;

    export interface SendUrlProcessorDefinition {
        type: "send";
        url: string;
    }

    export interface FieldComponent {
        registry: Registry;
        definition: FieldDefinition;
    }

    export interface Registry {
        fields: { [typeName: string]: string };
    }

    export interface UpdateStateEvent {
        state: StateNode | StateLeaf;
        valueChanged?: boolean;
    }
}
