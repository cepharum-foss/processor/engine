import DefaultTheme from "vitepress/theme";
import FormsProcessor from "../../../src/components/forms-processor.vue";

/** @type {import("vitepress").Theme} */
export default {
	extends: DefaultTheme,
	enhanceApp( { app } ) {
		app.component( "FormsProcessor", FormsProcessor );
	}
};
